# Define variabel acces key
variable "access_key" {
  type        = string
  sensitive   = true
}

# Define variabel secret key
variable "secret_key" {
  type        = string
  sensitive   = true
}
